# emails to bugs

report_blocked_win_ad_user is a windows service witch reports blocked ad users
to a specified ms teams chanel via webhook.

## Installation

Clone repository

git clone https://gitlab.com/uath4u/report_locked_ad_user_to_teams_channel.git

Install dependencies with pip.

pip3 install requirements.txt

## Configuration

Search for

- DOMAIN CONTROLER NAME
- TEAMS WEBHOOK HERE

and replace it with your domain controler name and teams webhook address.

Add all accounts you don't want to get a blocked notification in
account_ignore.cfg

## Usage

You need to have admin access.

Register your service:

- python script_name.py install

Update your servicemanager:

- python script_name.py update

Start your servicemanager:

- python script_name.py start

Stop your servicemanager:

- python script_name.py stop
