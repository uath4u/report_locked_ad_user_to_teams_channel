import pymsteams
import win32evtlog
import win32serviceutil
import win32service
import win32event
import servicemanager
import socket
import pytz

from bs4 import BeautifulSoup
from datetime import datetime
from os import path
from pathlib import Path


class ReportBlockedUser(win32serviceutil.ServiceFramework):
    """Main Class to submit eventvalues of event 4740 to teams chanel

    Public Modules

    SvcStop:                            Stops the windowsservice
    SvcDoRun:                           Starts the windowsservice
    main:                               Mainfunction triggered by SvcDoRun
    subscribe_to_event_blocked_user:     Subscribes to evtlog of domain controler
    new_logs_event_handler:             Function witch is called if event 4740 is triggered
    get_event_values:                   Parse eventxml to collect necessary informations
    build_connectioncard:               Creates the connectioncard send to teams
    send_connectioncard:                sends connectioncard to teams
    parse_command_line:                 Classmethod to install the service

    Attributes

    self.hWaitStop:                     Windowsevent set if Service is stopped
    self.account_name:                  Accountname witch was logged
    self.target_domain_name:            Name of the Computer witch has logged the Account
    self.caller_computer:               Name of the Computer the login was tried from
    self.date:                          Time and date of the event
    self.account_ignore_list:           List of Accounts not to report
    self.subscription:                  Object containing the eventsubscription
    self.is_running:                    Object to control the run of the service
    """
    _svc_name_ = 'ReportLockedUser'
    _svc_display_name_ = 'Report locked user'
    _svc_description_ = 'Reports locked users from ad to teams'

    def __init__(self, args):
        """Instantiating Class EmailToBug

        self.hWaitStop sets up as an instance of wind32event.Event
        self.account_name sets up as instance of string
        self.target_domain_name sets up as instance of string
        self.caller_computer sets up as instance of string
        self.date sets up as instance of string
        self.account_ignore_list sets up as instance of list
        self.subscription sets up as instance of list
        self.is_running sets up as instance of boolean
        """

        win32serviceutil.ServiceFramework.__init__(self, args)
        self.hWaitStop = win32event.CreateEvent(None, 0, 0, None)
        socket.setdefaulttimeout(60)

        self.account_name = ''
        self.target_domain_name = ''
        self.caller_computer = ''
        self.date = ''
        self.account_ignore_list = self.get_account_ignore_list()

        self.subscription = ''
        self.is_running = True

    def SvcStop(self):
        """DO NOT RENAME FUNCTION!!!

        SvcStop(self) -> None

        Function witch is called when the service is stopped"""

        self.is_running = False

        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        win32event.SetEvent(self.hWaitStop)

    def SvcDoRun(self):
        """DO NOT RENAME FUNCTION!!!

        SvcDoRun(self) -> None

        Function witch is called when the service is started"""

        self.is_running = True

        self.ReportServiceStatus(win32service.SERVICE_RUNNING)
        servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE, servicemanager.PYS_SERVICE_STARTED,
                              (self._svc_name_, ''))
        self.main()

    def main(self):
        """Mainfunction witch is called by SvcDoRun.

        main(self) -> None

        Connecting to eventlog of domain controler by calling self.subscribe_to_event_blocked_user
        und after starts an endless loop.
        """

        self.subscribe_to_event_blocked_user()

        while self.is_running:
            pass

    @staticmethod
    def get_account_ignore_list():
        """"Gets the list of account not to report to teams from account_ignore.cfg.

        get_account_ignore_list() -> list (containing the names of the accounts

        Opens the file and returns the list of accounts"""

        with open(path.join(Path(__file__).resolve().parent, 'account_ignore.cfg')) as config_file:

            return [account.replace('\n', '') for account in config_file.readlines()
                    if account.replace('\n', '') and '#' not in account]

    def subscribe_to_event_blocked_user(self):
        """Subscribes to event 4740 and sets self.new_logs_event_handler as handler for event

        sebscribe_to_event_create_user(self) -> None

        Function

        Subscribes to event 4740 on evtlog of the domain controler using win32evtlog.
        """

        event_context = {"info": "User has been locked"}
        event_source = 'security'

        remote_session = win32evtlog.EvtOpenSession(('INSERT DAMAIN CONTROLER NAME', None, None, None))

        self.subscription = win32evtlog.EvtSubscribe(event_source, win32evtlog.EvtSubscribeToFutureEvents, None,
                                                     Callback=self.new_logs_event_handler, Context=event_context,
                                                     Query='Event/System[EventID=4740]', Session=remote_session)

    def new_logs_event_handler(self, reason, context, evt):
        """Called when a new events are logged.

        new_logs_event_handler(self, reason -> string, context -> string, evt -> string) -> None

        Arguments

        reason:      reason why the event was logged?
        context:     context the event handler was registered with
        evt:         event handle

        Function

        The function collects the eventxml and calls self.send_connectioncard()
        """
        self.get_event_values(evt=evt)

        if self.account_name not in self.get_account_ignore_list():
            self.send_connectioncard(self.build_connectioncard())

    def get_event_values(self, evt):
        """Get the necessary values of the event

        get_event_values(self, evt -> string) -> None

        Function

        Gets the eventxml and parse the necessary values"""

        event_xml = win32evtlog.EvtRender(evt, win32evtlog.EvtRenderEventXml)
        xml_tree = BeautifulSoup(event_xml, 'lxml')
        self.account_name = xml_tree.find('data', {'name': 'TargetUserName'}).contents[0]
        self.target_domain_name = xml_tree.find('data', {'name': 'TargetDomainName'}).contents[0]
        self.caller_computer = xml_tree.find('computer').contents[0]
        self.date = (datetime.strptime(xml_tree.find('timecreated').get('systemtime')[0:19].replace('T', ' '),
                                       '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc).
                     astimezone(pytz.timezone('Europe/Berlin'))).strftime('%Y-%m-%d %H:%M:%S')

    def build_connectioncard(self):
        """Build the connectioncard send to teams connector.

        build_connectioncard(self) -> string (containing html for connectioncard) OR Exception if exception is raised.

        Function

        The Function build html-text from eventvalues and returns it to caller.
        """

        try:
            message = pymsteams.connectorcard('https://INSERT TEAMS WEBHOOK HERE')

            message.color('FF4040')

            message.title(f'<strong style="color:red">Benutzer {self.account_name} gesperrt</strong>')
            message.text(f' ')

            info_section = pymsteams.cardsection()

            info_section.text(f'<table>'
                              f'<head>'
                              f'<tr>'
                              f'<th>Benutzer</th>'
                              f'<th>Rechner</th>'
                              f'<th>Datum</th>'
                              f'</head>'
                              f'<tbody>'
                              f'<tr>'
                              f'<td>{self.account_name}</td>'
                              f'<td>{self.target_domain_name}</td>'
                              f'<td>{self.date}</td>'
                              f'</tr>'
                              f'</tbody>'
                              f'</table>')

            message.addSection(info_section)
            return message

        except Exception as exc:

            return exc

    @staticmethod
    def send_connectioncard(message_card):
        """Sending connectioncard to teamsconnector

        send_connectioncard(message_card -> string) -> None

        Argument

        message_card:   pymsteams.message object

        Function

        Sends connectioncard to teamsconnector using messageobject.send()"""

        message_card.send()

    @classmethod
    def parse_command_line(cls):
        """ClassMethod to parse the command line

        pars_command_line(cls) -> None

        Function

        Reads commandline and instantiating ReportBlockedUser
        """
        win32serviceutil.HandleCommandLine(cls)


if __name__ == '__main__':

    ReportBlockedUser.parse_command_line()
    exit()
